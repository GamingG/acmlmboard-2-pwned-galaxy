<?php

	require 'lib/common.php';
	
	if (!has_perm('ip-history-and-search'))
		error('Error', "You have no permissions to do this!<br> <a href=./>Back to main</a>");
	
	$userid = isset($_GET['id']) ? (int)$_GET['id'] : 0;
	$user = $sql->fetchp("SELECT " . userfields() . " FROM `users` WHERE `id` = ?", array($userid));
	if (!$user)
		error("Error", "Invalid user ID.");
				
	pageheader('IP history');
	print "<a href=\"./\">Main</a> - IP history for " . userdisp($user) . "
           <br><br>
";	
	
	$iphistoryquery = $sql->prepare("SELECT * FROM `iphistory` WHERE `user` = ? ORDER BY date DESC", array($userid));
	
	print 	"<table cellspacing=\"0\" class=\"c1\">
".			"  <tr class=\"h\">
".			"    <td class=\"b h\">Date
".			"    <td class=\"b h\">Old IP
".			"    <td class=\"b h\">New IP
";

	if ($sql->numrows($iphistoryquery)) {
		$_TR = 1;
		while ($iphistory = $sql->fetch($iphistoryquery)) {
			$_TR = ($_TR == 1) ? 2 : 1;
			print 	"  <tr class=\"n$_TR\">
".					"    <td class=\"b n3\" align=\"center\">".cdate($dateformat, $iphistory['date']) . "
".					"    <td class=\"center b\" align=\"center\">".ipformat($iphistory['oldip']) . "
".					"    <td class=\"center b\" align=\"center\">".ipformat($iphistory['newip']) . "
";
		}
	}
	
	print 	"</table>
";

	pagefooter();

?>