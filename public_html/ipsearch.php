<?php
	require 'lib/common.php';
	if (!has_perm('ip-history-and-search')) error('Error', "You have no permissions to do this!<br> <a href=./>Back to main</a>");
	
	
	pageheader('IP search');
	
	$ip = isset($_REQUEST['ip']) ? htmlspecialchars($_REQUEST['ip']) : '';
	
	print 	"<a href='./'>Main</a> - <a href=\"management.php\">Management</a> - IP search<br><br>
".			"<form action=\"\" method=\"GET\">
".			"	<table cellspacing=\"0\" class=\"c1\">
".			"		<tr class=\"h\"><td class=\"b h\" colspan=2>IP search
".			"		<tr>
".			"			<td class=\"center b n1\">IP:<br><span class=sfont>% acts as a generic wildcard.</span>
".			"			<td class=\"b n2\"><input type=\"text\" name=\"ip\" size=30 maxlength=50 value=\"" . htmlspecialchars($ip) . "\">
".			"		<tr>
".			"			<td class=\"b n1\">&nbsp;
".			"			<td class=\"b n1\"><input type=\"submit\" class=\"submit\" value=\"Search\">
".			"	</table>
".			"</form>
";

	if ($ip) {
		$_ip = urlencode($ip);
		print 	"<br>
".				"<table cellspacing=\"0\" class=\"c2\">
".				"	<tr><td class=\"b n3\">Tools: <a href=\"ipbans.php?ip=$_ip\">Ban</a> | <a href=\"http://dnsquery.org/ipwhois/$_ip\" target=\"_blank\">Lookup</a>
".				"</table>
";
		
		$lm = $sql->prepare("SELECT `u`.`ip`, " . userfields() . ", `u`.`lastview` `date` FROM `users` `u` WHERE `u`.`ip` LIKE ? ORDER BY `date` DESC", array($ip));
		print 	"<br>
".				"<table cellspacing=\"0\" class=\"c1\">
".				"	<tr class=\"h\"><td class=\"b h\" colspan=3>Latest matches
".				"	<tr class=\"c\"><td class=\"b c\">Date<td class=\"b c\">IP<td class=\"b c\">User
";	
		        if (!$sql->numrows($lm)) {
			print 	"	<tr class=\"n1\"><td class=\"center b\" colspan=3>No matches.
".					"</table>
";
                        }		
		        $_C = 1;
		        while ($_lm = $sql->fetch($lm)) {
			print 	"	<tr class=\"n$_C\">
".					"		<td class=\"center b\" align=\"center\">".cdate($dateformat, $_lm['date']) . "
".					"		<td class=\"center b\" align=\"center\">".htmlspecialchars($_lm['ip']) . "
".					"		<td class=\"center b\" align=\"center\">".userlink($_lm)."
";
                        $_C = ($_C == 1) ? 2 : 1;
                        }		
		print 	"</table>
";

		$hist = $sql->prepare("SELECT `h`.`newip` `ip`, " . userfields() . ", `h`.`date` FROM `iphistory` `h` LEFT JOIN `users` `u` ON `u`.`id` = `h`.`user` WHERE `h`.`newip` LIKE ? ORDER BY `date` DESC", array($ip));
		print 	"<br>
".				"<table cellspacing=\"0\" class=\"c1\">
".				"	<tr class=\"h\"><td class=\"b h\" colspan=3>IP history matches
".				"	<tr class=\"c\"><td class=\"b c\">Date<td class=\"b c\">IP<td class=\"b c\">User
";	
		        if (!$sql->numrows($hist)) {
			print 	"	<tr class=\"n1\"><td class=\"center b\" colspan=3>No matches.
".					"</table>
";
		        }		
		        $_C = 1;
		        while ($_hist = $sql->fetch($hist)) {
			print 	"	<tr class=\"n$_C\">
".					"		<td class=\"center b\" align=\"center\">".cdate($dateformat, $_hist['date']) . "
".					"		<td class=\"center b\" align=\"center\">".htmlspecialchars($_hist['ip']) . "
".					"		<td class=\"center b\" align=\"center\">".userlink($_hist)."
";
			$_C = ($_C == 1) ? 2 : 1;
		        }		
		print 	"</table>
";
		
		if (strpos($ip, '%') === FALSE) {
			$no = '<span style="color:#4f4;">Negative</span>';
			$yes = '<span style="color:#f44;">Positive</span>';
				
			$sfs = $no;
			$tor = $no;
			$tor2 = $no;

			$result = QueryURL('http://www.stopforumspam.com/api?ip='.urlencode($ip));
			if ($result && stripos($result, '<appears>yes</appears>') !== FALSE)
				$sfs = $yes;
				
			if (IsTorExitPoint($ip))
				$tor = $yes;
				
			$torlist = QueryURL('https://check.torproject.org/exit-addresses');
			if (stripos($torlist, 'ExitAddress ' . $ip) !== FALSE)
				$tor2 = $yes;
				
			print 	"<br>
".					"<table cellspacing=\"0\" class=\"c1\">
".					"	<tr class=\"h\"><td class=\"b h\" colspan=2>Lamer checks
".					"	<tr>
".					"		<td class=\"center b n1\">StopForumSpam
".					"		<td class=\"b n2\"align=\"left\">$sfs
".					"	<tr>
".					"		<td class=\"center b n1\">Tor
".					"		<td class=\"b n2\" align=\"left\">$tor
".					"	<tr>
".					"		<td class=\"center b n1\">Tor 2
".					"		<td class=\"b n2\" align=\"left\">$tor2
".					"</table>
";
		}
	}
	
	pagefooter();

?>