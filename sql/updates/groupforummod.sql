#Allows groups to be assigned as forum moderators.
#Date: 11/4/2015

ALTER TABLE `forummods` CHANGE `uid` `id` INT(12) NOT NULL;ALTER TABLE `forummods` ADD `type` INT(1) NOT NULL DEFAULT '0';