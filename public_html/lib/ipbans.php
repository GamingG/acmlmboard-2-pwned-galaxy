<?php
  //ipbans.php - core for new IP ban functions, started 2007-02-19 // blackhole89

  //delete expired IP bans
  $sql->prepare('DELETE FROM ipbans WHERE expires < ? AND expires > ?', array(ctime(), '0'));

  //actual ban checking
  $r=$sql->prepare("SELECT * FROM ipbans WHERE ? LIKE ipmask", array($userip));
  if(@$sql->numrows($r)>0)
  {

    // report the IP as banned like before
    if ($loguser) $sql -> prepare("UPDATE `users` SET `ipbanned` = ? WHERE `id` = ?", array('1', $loguser['id']));
    else $sql -> prepare("UPDATE  `guests` SET `ipbanned` = ? WHERE `ip` = ?", array('1', $_SERVER['REMOTE_ADDR']));

    //a ban appears to be present. check for type
    //and restrict user's access if necessary
    $i=$sql->fetch($r);
    if($i['hard'])
    {
      //hard IP ban; always restrict access fully

//	  header("Location: http://banned.ytmnd.com/");
//	  header("Location: http://board.acmlm.org/");
	  // fuck this shit
	  
      pageheader('IP banned');
      print
          "<table cellspacing=\"0\" class=\"c1\">
".        "  <tr class=\"n2\">
".        "    <td class=\"b n1\" align=\"center\">
".        "      Sorry, but your IP address appears to be banned from this board.
".        "</table>
";
      pagefooter();
      die();
	  
    } else if(!$i['hard'] && (!$log || is_banned_gid($loguser['group_id']))) {
      //"soft" IP ban allows non-banned users with existing accounts to log on
      if(!strstr($_SERVER['PHP_SELF'],"login.php"))
      {
        pageheader('IP restricted');
        print
          "<table cellspacing=\"0\" class=\"c1\">
".        "  <tr class=\"n2\">
".        "    <td class=\"b n1\" align=\"center\">
".        "      Access from your IP address to this board appears to be limited.<br>
".        "      <A HREF=login.php>Login</A>
".        "</table>
";
        pagefooter();
        die();
      }
    }
  }
?>