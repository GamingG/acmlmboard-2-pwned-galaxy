<?php
require 'lib/common.php';

if (!has_perm('view-log')) error("Error", "You have no permissions to do this!<br> <a href=./>Back to main</a>");

pageheader('Board log');

print "<table cellspacing=\"0\" class=\"c1\">
".      "  <tr class=\"h\">
".      "    <td class=\"b h\" width=15%>Date</td>
".      "    <td class=\"b h\" width=65%>Action</td>
".      "    <td class=\"b h\" width=10%>IP</td>
";

  $actlogs = $sql->query("SELECT * FROM boardlog ORDER BY id DESC LIMIT 100");
  while($actlog = $sql->fetch($actlogs)){
    print
        "  <tr align=\"center\">
".      "    <td class=\"b n1\">" . cdate($dateformat, $actlog['date']) . "</td>
".      "    <td class=\"b n2\">{$actlog['acttext']}</td>
".      "    <td class=\"b n2\">" . ipformat($actlog['ip']) . "</td>
";
  }
  print "</table>";

pagefooter();
?>