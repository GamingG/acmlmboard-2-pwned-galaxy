#Adds the Large Matte Painting badge for larger avatars and the Spiritual Calculator for more mood avatars 
#Date: 11/18/2015

INSERT INTO `badges` (`id`, `image`, `priority`, `type`, `name`, `description`, `inherit`, `posttext`, `effect`, `effect_variable`, `coins`, `coins2`) 
VALUES (LAST_INSERT_ID(), 'img/badges/avatarphoto.png', '50', '2', 'Large Matte Painting', 'Shows the need to make a larger presence on the world.', NULL, NULL, 'larger-avatar', NULL, '2500', '0'), 
(LAST_INSERT_ID(), 'img/badges/calculator.png', '50', '2', 'Spiritual Calculator', 'More mood avatars for when you''re feeling moody.', NULL, NULL, 'increase-num-moods', NULL, '2500', '0');