<?php

  //2007-07-01 blackhole89
  //xkeeper: fadding width/height to make it load better, adding align to move it away from the IP somewhat
  function flagip($flagparam){
    global $sql; 
    return ($flagparam['cc2'] ? " <img src=\"img/flags/" . strtolower($flagparam['cc2']) . ".png\" width=\"16\" height=\"11\" align=\"right\">" : "") . ipformat($flagparam['ip']);
  }

  function feedicon($icon, $para, $text = "RSS feed"){
    return "<a href='$para'><img src='$icon' border='0' style='margin-right:5px' title='$text'></a>"
          . "<link rel='alternate' type='application/rss+xml' title='$text' href='$para'>";
  }

  function ipformat($ip){
    if (has_perm('ip-history-and-search')) return "<a href=\"ipsearch.php?ip=$ip\">$ip</a>";
    return $ip;
  }

  function timelink($gettime, $timex, $url){
    return ($gettime == $timex ? " " . timeunits2($timex) . " " : " <a href=$url=$timex>" . timeunits2($timex) . '</a> ');
  }

?>