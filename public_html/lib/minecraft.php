<?php
function minecraftNameToID($name)
{
	$response = json_decode(file_get_contents("https://api.mojang.com/users/profiles/minecraft/{$name}"), true);

	return $response['id'];
}

function minecraftIDToName($id)
{
	$response = json_decode(file_get_contents("https://api.mojang.com/user/profiles/{$id}/names"), true);

	return $response[0]['name'];
}

function userIDToMCID($id)
{
	global $sql;
	
	return @$sql->result($sql->query("SELECT mcid FROM user_minecraft WHERE uid=".intval($id)));
}

define("MINECRAFT_COLOR_CHARACTER", "�");
?>