<?php
  require 'function.php';

  $t = isset($_GET['t']) ? $_GET['t'] : '';
  $t = str_replace(' ', '', $t);
  $r = isset($_GET['r']) ? (int)$_GET['r'] : 0;

  if($t){
    $sql->prepare("INSERT INTO rpgchat (chan, date, user, text) "
               ."VALUES (?, ?, ?, ?)", array($r, ctime(), $uid, $t));
  }

  $out = '';
  $lines = $sql->prepare("SELECT r.id, r.date, u.name, r.text "
                    ."FROM rpgchat r "
                    ."LEFT JOIN users u ON r.user = u.id "
                    ."WHERE r.id > ? AND chan = ? "
                    ."ORDER BY r.id DESC LIMIT 40", array($_GET['d'], $r));

  $maxid = 0;
  $rows = 0;
  while($line = $sql->fetch($lines)){
    if(!$maxid)
      $maxid = $line['id'];

    $line['text'] = wordwrap($line['text'], 34, '�br�', true);
    $rows += substr_count($line['text'], '�br�');

    $line['name'] = str_replace(' ', '�', $line['name']);
    $line['text'] = str_replace(' ', '�', $line['text']);
    $line['text'] = str_replace('�br�', ' � � ', $line['text']);

    $out = cdate('H:i', $line['date']) . ' ' . $line['name'] . ' ' . $line['text'] . ' ' . $out;
    $rows++;
  }
  print "$rows $maxid $out";
?>