<?php
  require 'lib/common.php';

  function sslicon($a, $uid = 0) {
    if(has_perm('view-post-ips') && $a) {
      return "<img src='img/ssloff.gif'>";
    }
    return "";
  }

  pageheader('Online users');

  $time = isset($_GET['time']) ? (int)$_GET['time'] : 300;


  $hiddencheck  = "AND u.hidden = 0 ";
  if (has_perm('view-hidden-users')) {
    $hiddencheck = "";
  }

  $users = $sql->prepare("SELECT u.*, ip.cc2 FROM users u "
                    ."LEFT JOIN ip2c ip ON ip.ip_from <=  inet_aton(u.ip) AND ip.ip_to >=  inet_aton(u.ip) "
                    ."WHERE u.lastview > ? $hiddencheck"
                    ."ORDER BY u.lastview DESC", array((ctime() - $time)));

  $guests = $sql->prepare("SELECT g.*, ip.cc2 FROM guests g "
                    ."LEFT JOIN ip2c ip ON ip.ip_from <=  inet_aton(g.ip) AND ip.ip_to >=  inet_aton(g.ip) "
                    ."WHERE g.date > ? "
                    ."AND g.bot = ? "
                    ."ORDER BY g.date DESC", array((ctime()-$time), '0'));
  $bots = $sql->prepare("SELECT b.*, ip.cc2 FROM guests b "
                   ."LEFT JOIN ip2c ip ON ip.ip_from <=  inet_aton(b.ip) AND ip.ip_to >=  inet_aton(b.ip) "
                    ."WHERE b.date > ? "
                    ."AND b.bot = ? "
                    ."ORDER BY date DESC", array((ctime()-$time), '1'));

  print "<table cellspacing=\"0\" width=100%>
".      "  <td class=\"nb\">Online users during the last ".timeunits2($time).":</table>
".      '<div style="margin-left: 3px; margin-top: 3px; margin-bottom: 3px; display:inline-block">
'.       timelink($time, 60, "online.php?time").'|'.timelink($time, 300, "online.php?time").'|'.timelink($time, 900, "online.php?time").'|'.timelink($time, 3600, "online.php?time").'|'.timelink($time, 86400, "online.php?time")."</div>
".      "<table cellspacing=\"0\" class=\"c1\">
".      "  <tr class=\"h\">
".      "    <td class=\"b h\" width=30>#</td>
".      "    <td class=\"b h\">Name</td>
".      "    <td class=\"b h\" width=90>Last view</td>
".      "    <td class=\"b h\" width=140>Last post</td>
".      "    <td class=\"b h\" width=140>Last Game Activity</td>
". (has_perm('view-user-urls') ? 
        "    <td class=\"b h\">URL</td>" : '') . "
". (has_perm('view-post-ips') ? 
        "    <td class=\"b h\" width=120>IP</td>" : '') . "
".      "    <td class=\"b h\" width=50>Posts</td>
";
  for($i = 1; $user = $sql->fetch($users); $i++){
    $user['ssl'] = "";
    if($user['url'][0] == '!') {
      $user['ssl'] = 0;
      $user['url'] = substr($user['url'], 1);
      $user['ssl'] = 1;
    }
    $tr = ($i % 2 ? 'n2' : 'n3');
    print "<tr class=\$tr\" align=\"center\">
".        "    <td class=\"b n1\">$i.</td>
".        "    <td class=\"b\" align=\"left\">" . ($user['hidden'] ? '(' . userlink($user) . ')' : userlink($user)) . "</td>
".        "    <td class=\"b\">" . cdate($loguser['timeformat'], $user['lastview']) . "</td>
".        "    <td class=\"b\">" . ($user['lastpost'] ? cdate($dateformat, $user['lastpost']) : '-') . "</td>
".(has_perm('view-user-urls') ?        
          "    <td class=\"b\" align=\"left\"><span style='float:right'>" . sslicon($user['ssl'], $user['id']) . "</span" . ($user['url'] ? "><a href={$user['url']}>" . str_replace(array("%20", "_"), " ", $user['url']) . "</a>" : '>-') . ($user['ipbanned'] ? " (IP banned)" : "") . "</td>" : '') . "
".        "    <td class=\"b\">".($user[lastgamedesc]?$user[lastgamedesc]:'-')."</td>":'')."
". (has_perm("view-post-ips") ? 
          "    <td class=\"b\">" . flagip($user) . "</td>" : '') . "
".        "    <td class=\"b\">{$user['posts']}</td>
";
  }
  print "</table>
".      "<br>
".      "<table cellspacing=\"0\" width=100%>
".      "  <td class=\"nb\">Guests:</table>
".      "<table cellspacing=\"0\" class=\"c1\">
".      "  <tr class=\"h\">
".      "    <td class=\"b h\" width=30>#</td>
".      "    <td class=\"b h\" width=70 style=\"min-width: 150px;\">User agent (Browser)</td>
".      "    <td class=\"b h\" width=70>Last view</td>
".      "    <td class=\"b h\">URL</td>
".(has_perm("view-post-ips") ? 
        "    <td class=\"b h\" width=120>IP</td>" : '') . "
";
  for($i = 1; $guest = $sql->fetch($guests); $i++){
    $guest['ssl'] = "";
    if($guest['url'][0] == '!') {
      $guest['ssl'] = 0;
      $guest['url'] = substr($guest['url'], 1);
      $guest['ssl'] = 1;
    }
    $tr = ($i % 2 ? 'n2' : 'n3');
    print "<tr class=\$tr\" align=\"center\">
".        "    <td class=\"b n1\">$i.</td>
".        "    <td class=\"b\" align=\"left\"><span title=\"" . htmlspecialchars($guest['useragent']) . "\" style=white-space:nowrap>" . htmlspecialchars(substr($guest['useragent'], 0, 65)) . "</span></td>
".        "    <td class=\"b\">" . cdate($loguser['timeformat'], $guest['date']) . "</td>
".        "    <td class=\"b\" align=\"left\"><span style='float:right'>".sslicon($guest['ssl']) . "</span><a href={$guest['url']}>" . str_replace(array("%20", "_"), " ",  $guest['url']) . "</a>" . ($guest['ipbanned'] ? " (IP banned)" : "") . "</td>
". (has_perm("view-post-ips") ?
          "    <td class=\"b\">" . flagip($guest) . "</td>" : '') . "
";
  }
  print "</table>
";

  
  print "</table>
".      "<br>
".      "<table cellspacing=\"0\" width=100%>
".      "  <td class=\"nb\">Bots:</table>
".      "<table cellspacing=\"0\" class=\"c1\">
".      "  <tr class=\"h\">
".      "    <td class=\"b h\" width=30>#</td>
".      "    <td class=\"b h\" width=70>Bot</td>
".      "    <td class=\"b h\" width=70>Last view</td>
".      "    <td class=\"b h\">URL</td>
".(has_perm("view-post-ips") ? 
        "    <td class=\"b h\" width=120>IP</td>" : '') . "
";
  for($i = 1; $guest = $sql->fetch($bots); $i++){
    $guest['ssl'] = "";
    if($guest['url'][0] == '!') {
      $guest['ssl'] = 0;
      $guest['url'] = substr($guest['url'], 1);
      $guest['ssl'] = 1;
    }
    $tr = ($i % 2 ? 'n2' : 'n3');
    print "<tr class=\"$tr\" align=\"center\">
".        "    <td class=\"b n1\">$i.</td>
".        "    <td class=\"b\" align=\"left\"><span title=\"". htmlspecialchars($guest['useragent']) . "\" style=white-space:nowrap>" . htmlspecialchars(substr($guest['useragent'], 0, 50)) . "</span></td>
".        "    <td class=\"b\">" . cdate($loguser['timeformat'], $guest['date']) . "</td>
".        "    <td class=\"b\" align=\"left\"><span style='float:right'>".sslicon($guest['ssl'])."</span><a href={$guest['url']}>{$guest['url']}</a>" . ($guest['ipbanned'] ? " (IP banned)" : "") ."</td>
".(has_perm("view-post-ips") ? 
          "    <td class=\"b\">" . flagip($guest) . "</td>" : '') . "
";
  }
  print "</table>
";
  
  pagefooter();

?>