<?php

  function editthread($id,$title='',$forum=0,$icon='',$closed=-1,$sticky=-1,$delete=-1){
    global $sql;

    if($delete < 1){
      $set='';
      if($title!='') $set.=",title=\"$title\"";
      if($icon!='')  $set.=",icon=$icon";
      if($closed>=0) $set.=",closed=$closed";
      if($sticky>=0) $set.=",sticky=$sticky";
      $set[0]=' ';
      if(strlen(trim($set)) > 0 && !is_array($set)) $sql->prepare("UPDATE threads SET ? WHERE id = ?", array($set, $id));

      if($forum)
        movethread($id,$forum);
    }else{
    }
  }

  function movethread($id,$forum){
    global $sql;

    if(!$sql->resultp("SELECT COUNT(*) FROM forums WHERE id = ?", array($forum))) return;

    $thread=$sql->fetchp("SELECT forum,replies FROM threads WHERE id = ?", array($id));
    $sql->prepare("UPDATE threads SET forum = ? WHERE id = ?", array($forum, $id));

    $last1=$sql->fetchp("SELECT lastdate,lastuser,lastid "
                       ."FROM threads "
                       ."WHERE forum = ? "
                       ."ORDER BY lastdate DESC LIMIT 1", array($thread['forum']));
    $last2=$sql->fetchp("SELECT lastdate,lastuser,lastid "
                       ."FROM threads "
                       ."WHERE forum = ? "
                       ."ORDER BY lastdate DESC LIMIT 1", array($forum));
    if($last1)
      $sql->prepare("UPDATE forums "
                ."SET posts = ?, "
                .    "threads = ?, "
                .    "lastdate = ?, "
                .    "lastuser = ?, "
				.    "lastid = ? "
                ."WHERE id = ?", array(posts-($thread['replies']+1), threads-1, $last1['lastdate'], $last1['lastuser'], $last1['lastid'], $thread['forum']));
    if($last2)
      $sql->prepare("UPDATE forums "
                 ."SET posts = ?, "
                 .    "threads = ?, "
                 .    "lastdate = ?, "
                 .    "lastuser = ?, "
				 .    "lastid = ? "
                 ."WHERE id = ?", array(posts+($thread['replies']+1), threads+1, $last2['lastdate'], $last2['lastuser'], $last2['lastid'], $forum));
  }
?>