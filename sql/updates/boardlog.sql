#Implements the boardlog column into the misc table and implements the boardlog table into the database
#Date: 10/28/2015

INSERT INTO `misc` (`field` , `intval` , `txtval` , `emailaddress` )VALUES ( 'boardlog', '5', '', '0' );

CREATE TABLE  `boardlog` (
`id` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`date` INT( 11 ) NOT NULL ,
`acttext` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`ip` VARCHAR( 15 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
) ENGINE = MYISAM ;