<?php
  require 'lib/common.php';
  needs_login(1);
  
//Permissions
  $lnkex = "";
  if(isset($_GET['user'])){
    $edid = $_GET['user'];
    $lnkex = "?user=$edid";
  } else {
    $edid = $loguser['id'];
  }
  $edid = (int)$edid;
  if(!can_edit_user_moods($edid)){
    error("Error", "You have no permissions to do this!<br> <a href=./>Back to main</a>");
  }
  
//Editing functionality
    if($config['badgesystem'] && has_badge_perm('larger-avatar', $edid)) { $adx = $avatardimx * 2; $ady = $avatardimy * 2; $asize = $avatarsize * 2; }
    else { $adx = $avatardimx; $ady = $avatardimy; $asize = $avatarsize; }
    if($config['badgesystem'] && has_badge_perm('increase-num-moods', $edid)) $moodnum = 128;
    else $moodnum = 64;

  if(isset($_POST['id']) && $_POST['a'] == "Save"){
    if(is_numeric($_POST['id'])){
      $fname = $_FILES['picture'];
      if($fname['size'] > 0){
        if($_POST['id']!=-1){
          $ava_out = img_upload($fname, "userpic/".$edid . "_" . $_POST['id'], $adx, $ady, $asize);
        } else {//Default Avatar
          $sql->prepare("UPDATE `users` SET `usepic` = `usepic`+1 WHERE `id` = ?", array($edid));
          $ava_out = img_upload($fname, "userpic/" . $edid, $adx, $ady, $asize);
        }
        if($ava_out == "OK!"){
          if($_POST['id'] != -1){ $sql->prepare("REPLACE INTO `mood` VALUES (?, ?, ?, ?, ?)", array($_POST['id'], $edid, $_POST['label'], 1, '')); }
        } else { $err .= $ava_out; }
      } else { //No file uploaded
        if(strlen($_POST['url']) > 0) {
          $img_data = getimagesize($_POST['url']); print_r($img_data);
          $ftypesv = array("png", "jpeg", "jpg", "gif");
          if($img_data[0] > $adx){ $err = "Image linked is too wide.<br>"; }
          if($img_data[1] > $ady){ $err .= "Image linked is too tall.<br>"; }
          if(!in_array(str_replace("image/", "", $img_data['mime']), $ftypes)){ $err .= "Image linked is not a gif, jpg or png file.<br>";}
          if(!$err){ $sql->prepare("REPLACE INTO `mood` VALUES (?, ?, ?, ?, ?)", array($_POST['id'], $edid, $_POST['label'], 0, $_POST['url'])); }
        } else {//No url specified.
          $sql->prepare("UPDATE `mood` SET `label` = ? WHERE `id` = ? AND `user` = ?", array($_POST['label'], $_POST['id'], $edid));
        }
      }
    } else { $err = "Bad id"; }
  if(!$err){ $err="Mood avatar updated."; }
  }
  if(isset($_POST['id']) && $_POST['a'] == "Delete"){
    if(is_numeric($_POST['id'])){
      if($_POST['id'] == -1){
        $sql->prepare("UPDATE `users` SET `usepic` = ? WHERE `id` = ?", array(0, $edid));
        echo "Default avatar set to blank.";
      } else {
        //Delete mood avatar
        $sql->prepare("DELETE FROM `mood` WHERE `id` = ? AND `user` = ?", array($_POST['id'], $edid));
        echo "Deleted.";
      }
    } else {
      echo "Bad id.";
    }
    die(); //Don't render page.
  }
  pageheader('Mood Avatar Editor 0.AB2');
//Various magic
  if(isset($err)){
    noticemsg("Notice", $err);
  }
  print "<script language=\"javascript\">
	function edit(av_id, av_lab, av_url)
	{
		document.getElementById(\"editpane\").style['display'] = \"inline\";
		document.getElementById(\"id\").value = av_id;
		document.getElementById(\"label\").value = av_lab;
		document.getElementById(\"url\").value = av_url;
		if(av_id==-1){
			document.getElementById(\"em\").style['display'] = \"none\";
			document.getElementById(\"em2\").style['display'] = \"none\";
		} else {
			document.getElementById(\"em\").style['display'] = \"\";
			document.getElementById(\"em2\").style['display'] = \"\";
		} 
	}
	function del(av_id, av_lab)
	{
		if(confirm(\"Are you sure you wish to delete \"+av_lab+\"?\")){
		y = new XMLHttpRequest();
		y.onreadystatechange = function()
		{
			if(y.readyState == 4)
			{
				if(y.responseText != \"OK\")
					alert(y.responseText);
				if(y.responseText == \"Deleted.\")
					document.getElementById(\"mood\"+av_id).style['display'] = \"none\";
				if(y.responseText == \"Default avatar set to blank.\")
					document.getElementById(\"defava\").style['background'] = \"none\";
			}
		};
		y.open('POST','mood.php$lnkex',true);
		y.setRequestHeader('Content-type','application/x-www-form-urlencoded');
		y.send('a=Delete&id='+av_id);
		}
	}
</script>";
  $avas = $sql->prepare("SELECT `u`.`usepic`, `m`.`id` `mid`, `m`.`url`, `m`.`label`, `m`.`local` FROM `users` `u` LEFT JOIN `mood` `m`  ON `m`.`user` = ? WHERE `u`.`id` = ?", array($edid, $edid));
//Default Avatar.
  $aurl= "";
  $u = $sql->fetch($avas);
  if($u['usepic'] >= 1){ $aurl="gfx/userpic.php?id=" . $edid . "&r=" . $u['usepic']; }
  print "<div style=\"margin: 4px; float: left; display:inline-block;\"><table cellspacing=\"0\" class=\"c1\">
  <tr class=\"h\">
    <td class=\"b h\">Default</td>
  </tr>
  <tr>
    <td class=\"b n2\"><div style=\"padding: 0px; margin: 0px; width: 180px; height: 180px; background: url($aurl) no-repeat center;\" id=\"defava\"></div>
  </tr><tr>
    <td class=\"b n1\"><a href=\"#\" onclick=\"edit(-1,'','')\">Edit</a> | <a href=\"#\" onclick=\"del(-1,'Default')\">Delete</a></td>
  </tr>
</table></div>";

//Mood Avatars
  $fid = 0;
  $lid = 0;
  for($i = 1; $mav = $sql->fetch($avas); $i++){
  if($lid != ($mav['mid'] - 1) && $fid == 0){ $fid = ($mav['mid'] - 1); } //Find a "free" ID.
  $lid = $mav['mid'];
  if($mav['local'] == 1){
    $aurl = "gfx/userpic.php?id=" . $edid . "_" . $mav['mid'];
  } else {
    $aurl = stripslashes($mav['url']);
  }
  print "<div style=\"margin: 4px; float: left; display:inline-block;\" id=\"mood" . $mav['mid'] . "\"><table cellspacing=\"0\" class=\"c1\">
  <tr class=\"h\">
    <td class=\"b h\">" . stripslashes($mav['label']) . "</td>
  </tr>
  <tr>
    <td class=\"b n2\"><div style=\"padding: 0px; margin: 0px; width: 180px; height: 180px; background: url(" . $aurl . ") no-repeat center;\"></div>
  </tr><tr>
    <td class=\"b n1\"><a href=\"#\" onclick=\"edit(" . $mav['mid'] . ", '" . htmlspecialchars($mav['label']) . "', '" . $mav['url'] . "')\">Edit</a> | <a href=\"#\" onclick=\"del(" . $mav['mid'] . ", '" . htmlspecialchars($mav['label']) . "')\">Delete</a></td>
  </tr>
</table></div>";
}
if($fid == 0){ $fid = $lid + 1; } //If no free ID.
if($fid <= $moodnum){
  print "<div style=\"margin: 4px; float: left; display:inline-block;\" id=\"mood64\"><table cellspacing=\"0\" class=\"c1\">
  <tr class=\"h\">
    <td class=\"b h\" style=\"width:180px;\">&nbsp</td>
  </tr>
  </tr><tr>
    <td class=\"b n1\"><a href=\"#\" onclick=\"edit(" . $fid . ", '(Label)', '')\">Add New</a></td>
  </tr>
</table></div>";
}
  print "<br clear=\"all\"><div id=\"editpane\" style=\"display:none;\"><form id=\"f\" action=\"mood.php$lnkex\" enctype=\"multipart/form-data\" method=\"post\"><table cellspacing=\"0\" class=\"c1\">
  <tr class=\"h\">
    <td class=\"b h\" colspan=2>Editing mood avatar</td>
  </tr>
  <tr id=\"em\">
    <td class=\"b n1\">Label</td>
    <td class=\"b n2\"><input type=\"text\" name=\"label\" id=\"label\" size=50 maxlength=100></td>
  </tr><tr>
    <td class=\"b n1\">Upload File</td>
    <td class=\"b n2\"><input type=\"file\" name=\"picture\" size=50></td>
  </tr><tr id=\"em2\">
    <td class=\"b n1\">Web link</td>
    <td class=\"b n2\"><input type=\"text\" name=\"url\" id=\"url\" size=50 maxlength=250></td>
  </tr><tr>
    <td class=\"b n1\"><input type=\"hidden\" name=\"id\" id=\"id\"></td>
    <td class=\"b n2\"><input type=\"submit\" name='a' value=\"Save\"></td>
  </tr></table></form></div>";
  pagefooter();
?>