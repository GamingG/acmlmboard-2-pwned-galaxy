<?php
  require 'lib/common.php';

  if (!has_perm('pending-users-panel'))
  {
	error("Error", "You have no permissions to do this!<br> <a href=./>Back to main</a>");
  }

  $act = isset($_GET['action']) ? $_GET['action'] : '';
  $err = '';

   if($act == "denyandipban") {
        if (!has_perm('deny-pending-users'))
        {
	      error("Error", "You have no permissions to do this!<br> <a href=./>Back to main</a>");
        }

	$id = unpacksafenumeric($_GET['id']);
	$pendinguser = $sql->fetchp("SELECT * FROM `pendingusers` WHERE `id` = ?", array($id));
	
	if (!$pendinguser)
		$err = "Cannot deny and IP ban pending user as the ID is invalid.";
	else
	{
		if (!$err)
		{ 
    $to = $pendinguser['email'];
    $subject = "{$config['boardtitle']} - Your account registration has been denied and you have been IP banned";
    $boardemail = $sql->resultq("SELECT `emailaddress` FROM `misc` WHERE `field` = 'boardemail'");
    $message = $pendinguser['name'] . ", your account at " . $config['base'] . " has been denied and you have been IP banned from the board. If you feel this was unjust, calmly contact the board admins at " . $boardemail . ".";
    $headers = "From: " . $boardemail . "\r\n"
			. "X-Mailer: PHP/" . phpversion();
    mail($to, $subject, wordwrap($message, 70), $headers);

     if ($boardlog >= 4) {
	       $sql->prepare("INSERT INTO boardlog SET date = ?, acttext = ?, ip = ?", array(ctime(),  userlink_by_id($loguser['id'], $config['showminipic']) . " denied {$pendinguser['name']}'s account registration and ip banned {$pendinguser['name']}", $loguser['ip']));
           $id = $sql->insertid();
     }

    $sql->prepare("INSERT INTO ipbans (ipmask, hard, expires, banner, reason) VALUES (?, ?, ?, ?, ?)", array($pendinguser['ip'], 1, 0, $loguser['name'], 'Denied and IP banned pending user'));
    $sql->prepare("DELETE FROM `pendingusers` WHERE `id` = ?", array($id));
               }
        }
    }else if($act == "deny" ) {
        if (!has_perm('deny-pending-users'))
        {
	      error("Error", "You have no permissions to do this!<br> <a href=./>Back to main</a>");
        }

	$id = unpacksafenumeric($_GET['id']);
	$pendinguser = $sql->fetchp("SELECT * FROM `pendingusers` WHERE `id` = ?", array($id));
	
	if (!$pendinguser)
		$err = "Cannot deny pending user as the ID is invalid.";
	else
	{
		if (!$err)
		{
    $to = $pendinguser['email'];
    $subject = "{$config['boardtitle']} - Your account registration has been denied";
    $boardemail = $sql->resultq("SELECT `emailaddress` FROM `misc` WHERE `field`='boardemail'");
    $message = $pendinguser['name'] . ", your account at " . $config['base'] . " has been denied. If you feel this was unjust, calmly contact the board admins at " . $boardemail . ".";
    $headers = "From: " . $boardemail . "\r\n"
			. "X-Mailer: PHP/" . phpversion();
    mail($to, $subject, wordwrap($message, 70), $headers);

     if ($boardlog >= 4) {
	       $sql->prepare("INSERT INTO boardlog SET date = ?, acttext = ?, ip = ?", array(ctime(),  userlink_by_id($loguser['id'], $config['showminipic']) . " denied {$pendinguser['name']}'s account registration", $loguser['ip']));
           $id = $sql->insertid();
     }

    $sql->prepare("DELETE FROM `pendingusers` WHERE `id` = ?", array($id));
               }
        }
    }else if($act == "approve" ) {
        if (!has_perm('approve-pending-users'))
        {
	      error("Error", "You have no permissions to do this!<br> <a href=./>Back to main</a>");
        }

	$id = unpacksafenumeric($_GET['id']);
	$pendinguser = $sql->fetchp("SELECT * FROM `pendingusers` WHERE `id`=?", array($id));
	
	if (!$pendinguser)
		$err = "Cannot approve pending user as the ID is invalid.";
	else
	{
		if (!$err)
		{
    $to = $pendinguser['email'];
    $subject = "{$config['boardtitle']} - Your account registration has been approved!";
    $boardemail = $sql->resultq("SELECT `emailaddress` FROM `misc` WHERE `field`='boardemail'");
    $message = $pendinguser['name'] . ", your account at " . $config['base'] . " has been approved. Please login at " . $config['base'] . "/login.php .";
    $headers = "From: " . $boardemail . "\r\n"
			. "X-Mailer: PHP/" . phpversion();
    mail($to, $subject, wordwrap($message, 70), $headers);

     if ($boardlog >= 4) {
	       $sql->prepare("INSERT INTO boardlog SET date = ?, acttext = ?, ip = ?", array(ctime(),  userlink_by_id($loguser['id'], $config['showminipic']) . " approved {$pendinguser['name']}'s account registration", $loguser['ip']));
           $id = $sql->insertid();
     }

    $userid = $sql->insertid();
    $sql->prepare("INSERT INTO users (id, name, pass, regdate, lastview, ip, sex, group_id, timezone, fontsize, theme, email, emailhide) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", array($userid, $pendinguser['name'], $pendinguser['pass'], $pendinguser['regdate'], $pendinguser['lastview'], $pendinguser['ip'], $pendinguser['sex'], is_default_gid('default'), $pendinguser['timezone'], $pendinguser['fontsize'], $pendinguser['theme'], $pendinguser['email'], $pendinguser['emailhide']));
    $sql->prepare("INSERT INTO usersrpg ($id) VALUES (?)", array($userid));
    $sql->prepare("INSERT INTO threadsread (uid, tid, time) SELECT ?, id, ? FROM threads", array($userid, $pendinguser['regdate']));
    $sql->prepare("INSERT INTO forumsread (uid, fid, time) SELECT ?, id, ? FROM forums", array($userid, $pendinguser['regdate']));
    sendirc("{irccolor-base}New user: \x0309".stripslashes($pendinguser[name])."{irccolor-base} - {irccolor-url}{boardurl}?u=$userid");
    sendirc("{irccolor-base}New user: \x0309".stripslashes($pendinguser[name])."{irccolor-base} - {irccolor-url}{boardurl}?u=$userid{irccolor-base} - [".$pendinguser['ip']."]",$config['staffchan']);
    $sql->prepare("DELETE FROM `pendingusers` WHERE `id` = ?", array($id));
               }
        }
    }

  pageheader('Pending Users');

  if($err) noticemsg("Error", $err);
        $pendingusers = $sql->query("SELECT * FROM `pendingusers` ORDER BY `id`");
print "<table cellspacing=\"0\" class=\"c1\">
".       "  <tr class=\"h\">
".       "    <td class=\"b h\">#</td>
".       "    <td class=\"b h\">Name</td>
".       "    <td class=\"b h\">Email</td>
".       "    <td class=\"b h\">Sex</td>
".       "    <td class=\"b h\">Timezone</td>
".       "    <td class=\"b h\">Registered on</td>
".       "    <td class=\"b h\">Last view</td>
".       "    <td class=\"b h\">IP</td>
".       "    <td class=\"b h\">Proxy?</td>
";
if (has_perm("approve-pending-users") || has_perm("deny-pending-users"))
print
         "    <td class=\"b h\">Actions</td>
";
    while($pendinguser = $sql->fetch($pendingusers)) {

$links = array();
if (has_perm("approve-pending-users")) $links[] = array('link' => "pendingusers.php?action=approve&id=" . urlencode(packsafenumeric($pendinguser['id'])), 'name' => 'Approve', 'message' => "approveConfirm(event)");
if (has_perm("deny-pending-users")) $links[] = array('link' => "pendingusers.php?action=deny&id=" . urlencode(packsafenumeric($pendinguser['id'])), 'name' => 'Deny', 'message' => "denyConfirm(event)");
if (has_perm("deny-pending-users")) $links[] = array('link' => "pendingusers.php?action=denyandipban&id=" . urlencode(packsafenumeric($pendinguser['id'])), 'name' => 'Deny and IP Ban', 'message' => "denyandipbanConfirm(event)");

$linkstext = '';
foreach ($links as $link) $linkstext .= ($linkstext ? ' | ' : '') . "<a href=\"{$link['link']}\" onclick=\"{$link['message']}\">{$link['name']}</a>";

  print "<script language=\"javascript\">
function approveConfirm(e) {
    if(confirm(\"Are you sure you want to approve pending user '{$pendinguser['name']}'?\"));
    else {
  e.preventDefault();
 }
}
</script>

<script language=\"javascript\">
function denyConfirm(e) {
    if(confirm(\"Are you sure you want to deny pending user '{$pendinguser['name']}'?\"));
    else {
  e.preventDefault();
 }
}
</script>

<script language=\"javascript\">
function denyandipbanConfirm(e) {
    if(confirm(\"Are you sure you want to deny and IP ban pending user '{$pendinguser['name']}'?\"));
    else {
  e.preventDefault();
 }
}
</script>";

if($pendinguser['sex'] == 0) $sex = "Male";
if($pendinguser['sex'] == 1) $sex = "Female";
if($pendinguser['sex'] == 2) $sex = "N/A";

      print "<tr>
".         "  <td class=\"b n1\" align=\"center\">{$pendinguser['id']}.</td>
".         "  <td class=\"b n2\" align=\"center\">{$pendinguser['name']}</td>
".         "  <td class=\"b n2\" align=\"center\">{$pendinguser['email']}</td>
".         "  <td class=\"b n2\" align=\"center\">$sex</td>
".         "  <td class=\"b n2\" align=\"center\">{$pendinguser['timezone']}</td>
".         "  <td class=\"b n2\" align=\"center\">" . cdate($dateformat,$pendinguser['regdate']) . "</td>
".         "  <td class=\"b n2\" align=\"center\">" . cdate($dateformat,$pendinguser['lastview']) . "</td>
".         "  <td class=\"b n2\" align=\"center\">" . ipformat($pendinguser['ip']) . "</td>
".         "  <td class=\"b n2\" align=\"center\"><font color=" . ($pendinguser['proxy'] ? "red>Yes" : "green>No") . "</font></td>
";
if (has_perm("approve-pending-users") || has_perm("deny-pending-users"))
print
         "  <td class=\"b n2\" align=\"center\">$linkstext</font></td>
";
    }
    print "</table>";


  pagefooter();

?>