<?php

require 'lib/common.php';

//Controls board settings - SquidEmpress
//Uses inspiration from Schezo's version in 1.92.08/Jul.
//Renamed 'Administrator Tools' as non root admins could be given lockdown access per perm system 

if (!has_perm('admin-tools-access'))
	error("Error", "You have no permissions to do this!<br> <a href=./>Back to main</a>");

$action = isset($_POST['action']) ? $_POST['action'] : '';

if ($action == "Apply changes") {
	$sql->prepare("UPDATE misc SET intval = ? WHERE field = 'adminapproval'", array($_POST['adminapproval']));
	$sql->prepare("UPDATE misc SET intval = ? WHERE field = 'boardlog'", array($_POST['boardlog']));
	$sql->prepare("UPDATE misc SET intval = ? WHERE field = 'regdisable'", array($_POST['regdisable']));
	$sql->prepare("UPDATE misc SET txtval = ? WHERE field = 'regdisable'", array($_POST['regdisabletext']));
	$sql->prepare("UPDATE misc SET intval = ? WHERE field = 'lockdown'", array($_POST['lockdown']));
	$sql->prepare("UPDATE misc SET txtval = ? WHERE field = 'lockdown'", array($_POST['lockdowntext']));
	$sql->prepare("UPDATE misc SET txtval = ? WHERE field = 'boardemail'", array($_POST['boardemail']));
	$sql->prepare("UPDATE misc SET intval = ? WHERE field = 'trashid'", array($_POST['trashid']));
	$sql->prepare("UPDATE misc SET txtval = ? WHERE field = 'boardtitle'", array($_POST['boardtitle']));
	$sql->prepare("UPDATE misc SET txtval = ? WHERE field = 'defaulttheme'", array($_POST['defaulttheme']));
	$sql->prepare("UPDATE misc SET intval = ? WHERE field = 'defaultfontsize'", array($_POST['defaultfontsize']));
	$sql->prepare("UPDATE misc SET intval = ? WHERE field = 'avatardimx'", array($_POST['avatardimx']));
	$sql->prepare("UPDATE misc SET intval = ? WHERE field = 'avatardimy'", array($_POST['avatardimy']));
	$sql->prepare("UPDATE misc SET intval = ? WHERE field = 'topposts'", array($_POST['topposts']));
	$sql->prepare("UPDATE misc SET intval = ? WHERE field = 'topthreads'", array($_POST['topthreads']));
	$sql->prepare("UPDATE misc SET intval = ? WHERE field = 'threadprevnext'", array($_POST['threadprevnext']));
	$sql->prepare("UPDATE misc SET intval = ? WHERE field = 'memberlistcolorlinks'", array($_POST['memberlistcolorlinks']));
	$sql->prepare("UPDATE misc SET intval = ? WHERE field = 'badgesystem'", array($_POST['badgesystem']));
	$sql->prepare("UPDATE misc SET intval = ? WHERE field = 'spritesystem'", array($_POST['spritesystem']));
	$sql->prepare("UPDATE misc SET intval = ? WHERE field = 'extendedprofile'", array($_POST['extendedprofile']));
	$sql->prepare("UPDATE misc SET intval = ? WHERE field = 'displayname'", array($_POST['displayname']));
	$sql->prepare("UPDATE misc SET intval = ? WHERE field = 'perusercolor'", array($_POST['perusercolor']));
	$sql->prepare("UPDATE misc SET intval = ? WHERE field = 'usernamebadgeeffects'", array($_POST['usernamebadgeeffects']));
	$sql->prepare("UPDATE misc SET intval = ? WHERE field = 'useshadownccss'", array($_POST['useshadownccss']));
	$sql->prepare("UPDATE misc SET intval = ? WHERE field = 'nickcolorcss'", array($_POST['nickcolorcss']));
	$sql->prepare("UPDATE misc SET intval = ? WHERE field = 'userpgnum'", array($_POST['userpgnum']));
	$sql->prepare("UPDATE misc SET intval = ? WHERE field = 'userpgnumdefault'", array($_POST['userpgnumdefault']));
	$sql->prepare("UPDATE misc SET intval = ? WHERE field = 'alwaysshowlvlbar'", array($_POST['alwaysshowlvlbar']));
	$sql->prepare("UPDATE misc SET intval = ? WHERE field = 'rpglvlbarwidth'", array($_POST['rpglvlbarwidth']));
	$sql->prepare("UPDATE misc SET txtval = ? WHERE field = 'atnname'", array($_POST['atnname']));

    if ($boardlog >= 4) {
	    $sql->prepare("INSERT INTO boardlog SET date = ?, acttext = ?, ip = ?", array(ctime(),  userlink_by_id($loguser['id'], $config['showminipic']) . " edited the Administrator Tools", $loguser['ip']));
        $id = $sql->insertid();
    }

	header('Location: administratortools.php?u=1');
	//exit('You should have been redirected...');
}

pageheader('Administrator Tools');

$updated = isset($_GET['updated']) ? $_GET['updated'] : 0;
if($updated) {
	noticemsg("Success", "The settings have been updated.");
}

// I don't know what was going on with some of these settings... 
// but I've changed them to conform to the standard previously used for settings.
//
// It is possible to use the same field name to store an int and text value, however this
// could cause confusion.

	$qforums = $sql->query("SELECT f.id, f.title, f.cat FROM forums f LEFT JOIN categories c ON c.id = f.cat ORDER BY c.ord, c.id, f.ord, f.id");
	$forums = array();
	while ($forum = $sql->fetch($qforums))
		$forums[$forum['id']] = $forum['title'];

print "<form action='administratortools.php' method='post' enctype='multipart/form-data'>
" . " <table cellspacing=\"0\" class=\"c1\">
" .
		catheader('Administrator tools') . "
" . "  <tr class=\"c\">
" . "    <td class=\"b h\" colspan=6>General settings</td>
" . "  <tr>
" . "    <td class=\"b n1\" align=\"center\">Board Title:</td>
" . "      <td class=\"b n2\"><input type=\"text\" name=\"boardtitle\" size=\"40\" maxlength=\"255\" value=\"" . htmlentities($configsql['boardtitle']['txtval']) . "\" class='right'></td>
" . fieldrow('Board Theme', fieldselect('defaulttheme', $configsql['defaulttheme']['txtval'], themelist())) . "
" . fieldrow('Trash Forum', fieldselect('trashid', $configsql['trashid']['intval'], $forums)) . "
" . "  <tr>
" . "    <td class=\"b n1\" align=\"center\">Board Font Size:</td>
" . "      <td class=\"b n2\"><input type=\"text\" name='defaultfontsize' size='3' maxlength='3' value='" . $configsql['defaultfontsize']['intval'] . "' class='right'></td>
" . "  <tr>
" . "    <td class=\"b n1\" align=\"center\">Avatar X Dimension:</td>
" . "      <td class=\"b n2\"><input type=\"text\" name='avatardimx' size='3' maxlength='3' value='" . $configsql['avatardimx']['intval'] . "' class='right'></td>
" . "  <tr>
" . "    <td class=\"b n1\" align=\"center\">Avatar Y Dimension:</td>
" . "      <td class=\"b n2\"><input type=\"text\" name='avatardimy' size='3' maxlength='3' value='" . $configsql['avatardimy']['intval'] . "' class='right'></td>
" . "  <tr>
" . "    <td class=\"b n1\" align=\"center\">Projected Date Posts:</td>
" . "      <td class=\"b n2\"><input type=\"text\" name='topposts' size='7' maxlength='7' value='" . $configsql['topposts']['intval'] . "' class='right'></td>
" . "  <tr>
" . "    <td class=\"b n1\" align=\"center\">Projected Date Threads:</td>
" . "      <td class=\"b n2\"><input type=\"text\" name='topthreads' size='7' maxlength='7' value='" . $configsql['topthreads']['intval'] . "' class='right'></td>
" . fieldrow('Memberlist Color Links', fieldoption('memberlistcolorlinks', $configsql['memberlistcolorlinks']['intval'], array('Disable', 'Enable'))) . "
" . fieldrow('Badge System', fieldoption('badgesystem', $configsql['badgesystem']['intval'], array('Disable', 'Enable'))) . "
" . fieldrow('Sprite System', fieldoption('spritesystem', $configsql['spritesystem']['intval'], array('Disable', 'Enable'))) . "
" . fieldrow('Extended Profile Fields (DO NOT USE; EXPERIMENTAL)', fieldoption('extendedprofile', $configsql['extendedprofile']['intval'], array('Disable', 'Enable'))) . "
" . fieldrow('Thread Prev Next Links', fieldoption('threadprevnext', $configsql['threadprevnext']['intval'], array('Disable', 'Enable'))) . "
" . fieldrow('Displaynames', fieldoption('displayname', $configsql['displayname']['intval'], array('Disable', 'Enable'))) . "
" . fieldrow('Custom Username Colors', fieldoption('perusercolor', $configsql['perusercolor']['intval'], array('Disable', 'Enable'))) . "
" . fieldrow('Username Badge Effects', fieldoption('usernamebadgeeffects', $configsql['usernamebadgeeffects']['intval'], array('Disable', 'Enable'))) . "
" . fieldrow('Username Shadow', fieldoption('useshadownccss', $configsql['useshadownccss']['intval'], array('Disable', 'Enable'))) . "
" . fieldrow('Theme Username Colors', fieldoption('nickcolorcss', $configsql['nickcolorcss']['intval'], array('Disable', 'Enable'))) . "
" . fieldrow('AB1.x Num Graphics', fieldoption('userpgnum', $configsql['userpgnum']['intval'], array('Disable', 'Enable'))) . "
" . fieldrow('All Themes Num Graphics', fieldoption('userpgnumdefault', $configsql['userpgnumdefault']['intval'], array('Disable', 'Enable'))) . "
" . fieldrow('EXP Level Bars', fieldoption('alwaysshowlvlbar', $configsql['alwaysshowlvlbar']['intval'], array('Disable', 'Enable'))) . "
" . "  <tr>
" . "    <td class=\"b n1\" align=\"center\">EXP Bar Size:</td>
" . "      <td class=\"b n2\"><input type=\"text\" name='rpglvlbarwidth' size='3' maxlength='3' value='" . $configsql['rpglvlbarwidth']['intval'] . "' class='right'></td>
" . "  <tr>
" . "    <td class=\"b n1\" align=\"center\">Attention Box Name:</td>
" . "      <td class=\"b n2\"><input type=\"text\" name='atnname' size='40' maxlength='255' value='" . $configsql['atnname']['txtval'] . "' class='right'></td>
" . "  <tr class=\"c\">
" . "    <td class=\"b h\" colspan=6>Miscellaneous settings</td>
" . fieldrow('Admin Approval Mode', fieldoption('adminapproval', $configsql['adminapproval']['intval'], array('Do not set', 'Set')))."
" . fieldrow('Board Log', fieldoption('boardlog', $configsql['boardlog']['intval'], array('No actions logged', 'Profile actions', 'Thread and post actions', 'Registration and login actions', 'Administrative actions', 'All actions logged')))."
" . fieldrow('Enable Registration', fieldoption('regdisable', $configsql['regdisable']['intval'], array('Enable', 'Disable'))) . "
" . "  <tr>
" . "    <td class=\"b n1\" align=\"center\">Disable Registering Message:</td>
" . "      <td class=\"b n2\"><textarea wrap=\"virtual\" name='regdisabletext' rows=8 cols=120>" . $configsql['regdisable']['txtval'] . "</textarea></td>
" . fieldrow('Enable Lockdown', fieldoption('lockdown', $configsql['lockdown']['intval'], array('Do not set', 'Set'))) . "
" . "  <tr>
" . "    <td class=\"b n1\" align=\"center\">Lockdown Message (Leave blank for default):</td>
" . "      <td class=\"b n2\"><textarea wrap=\"virtual\" name='lockdowntext' rows=8 cols=120>" . $configsql['lockdown']['txtval'] . "</textarea></td>
" . "  <tr>
" . "    <td class=\"b n1\" align=\"center\">Board Email:</td>
" . "      <td class=\"b n2\"><input type=\"text\" name='boardemail' size='40' maxlength='60' value='" . $configsql['boardemail']['txtval'] . "' class='right'></td>
" . "  <tr class=\"n1\">
" . "    <td class=\"b\">&nbsp;</td>
" . "    <td class=\"b\"><input type=\"submit\" class=\"submit\" name=action value='Apply changes'></td>
" . " </table>
";

pagefooter();
?>