#Adds a table for IP history.
#Date: 7/18/2014

CREATE TABLE `iphistory` (`user`INT( 11 ) NOT NULL ,`oldip` VARCHAR( 15 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,`newip` VARCHAR( 15 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,`date` INT( 11 ) NOT NULL) ENGINE = MYISAM DEFAULT CHARSET=utf8;