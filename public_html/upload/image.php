<?php
chdir("..");
require_once "./lib/common.php";

function dieWithError($msg)
{
	die(json_encode(array(
		"success" => false,
		"error" => $msg,
	)));
}

function dieWithSuccess($url)
{
	die(json_encode(array(
		"success" => true,
		"url" => $url,
	)));
}

function logResponse($msg)
{
	file_put_contents("log.txt", file_get_contents("log.txt").$msg."\n");
}

if(!$log)
	dieWithError("LOGIN_REQUIRED");

if(!count(@$_FILES))
	dieWithError("NO_UPLOAD");

$file = current($_FILES);

if($file['error'])
{
	switch($file['error'])
	{
		case 1:  dieWithError("EXCEEDED_SERVER_MAX");
		case 2:  dieWithError("EXDEEDED_CLIENT_MAX");
		case 3:  dieWithError("INCOMPLETE");
		case 4:  dieWithError("MISSING");
		default: dieWithError("UNKNOWN");
	}
}

if(!is_uploaded_file($file['tmp_name']))
	dieWithError("NOT_A_FILE");

if(!@getimagesize($file['tmp_name']))
	dieWithError("NOT_AN_IMAGE");

/* Imgur Shit */
$imgurAPI = array(
	"id" => PG_CONFIG_IMGUR_ID,
	"secret" => PG_CONFIG_IMGUR_SECRET,
);

$timeout = 30; // seconds

$headers = array(
	"Authorization: Client-ID {$imgurAPI['id']}",
);

$params = array(
	"image" => base64_encode(file_get_contents($file['tmp_name'])),
);

$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, 'https://api.imgur.com/3/image.json'); // .json gives us a JSON response
curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $params);

$out = curl_exec($ch);

curl_close ($ch);

logResponse($out);
$response = json_decode($out, true);
$url = $response['data']['link'];

/* End Imgur Shit */

if(!$url)
	dieWithError("EXTERNAL_SERVICE_EXCEPTION");

dieWithSuccess($url);
?>