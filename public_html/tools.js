var bState=new Array();

function wrapSelection(e,prefix,suffix)
{
	var el=document.getElementById(e);
	if(document.selection) {
		//IE-like
		el.focus();
		document.selection.createRange().text=prefix+document.selection.createRange().text+suffix;
	} else if(typeof el.selectionStart != undefined) {
		//FF-like
		el.value=el.value.substring(0,el.selectionStart)+prefix+el.value.substring(el.selectionStart,el.selectionEnd)+suffix+el.value.substring(el.selectionEnd,el.value.length);
		el.focus();
	}
}

function selectionLength(e)
{
	var el=document.getElementById(e);
	if(document.selection) return document.selection.createRange().text.length;
	else if(typeof el.selectionStart != undefined) { return el.selectionEnd-el.selectionStart; }
}

function buttonProc(e,bk,leadin,leadout)
{
	if(selectionLength(e)>0) wrapSelection(e,leadin,leadout);
	else {
		if(bState[bk]==1) {
			wrapSelection(e,"",leadout);
			bState[bk]=0;
			document.getElementById(bk).className="b n3";
		} else {
			wrapSelection(e,leadin,"");
			bState[bk]=1;
			document.getElementById(bk).className="b n1";
		}
	}
}

$(document).ready(function()
{
    var maxWidth = 640;
    var maxHeight = 640;
    
    $('img.from-bbcode').one('load', function()
    {
	$(this).data('_originalWidth',  $(this).width());
	$(this).data('_originalHeight', $(this).height());
	
	if($(this).width() < maxWidth && $(this).height() < maxHeight)
	{
	    $(this).data('_thumbWidth',  $(this).width());
	    $(this).data('_thumbHeight', $(this).height());
        }
        else if($(this).width() > $(this).height())
	{
	    $(this).data('_thumbWidth',  maxWidth);
	    $(this).data('_thumbHeight', $(this).height() * maxWidth / $(this).width());
        }
	else
	{
	    $(this).data('_thumbWidth',  $(this).width() * maxHeight / $(this).height());
	    $(this).data('_thumbHeight', maxHeight);
        }
	
	$(this).animate({
            width : $(this).data('_thumbWidth'),
            height : $(this).data('_thumbHeight')
        });
	
	this.src = imgurThumb(this.src);

	$(this).data('_expanded', false);
    }).each(function()
    {
        if(this.complete)
	    $(this).load();
    });
    
    $(document).on('click', 'img.from-bbcode', function()
    {
	if($(this).data('_expanded'))
	{
	    $(this).animate({
		width : $(this).data('_thumbWidth'),
		height : $(this).data('_thumbHeight')
	    });
	    
	    this.src = imgurThumb(this.src);
	}
	else
	{
	    this.src = imgurDethumb(this.src);

	    $(this).animate({
		width : $(this).data('_originalWidth'),
		height : $(this).data('_originalHeight')
	    });
	}
	
	$(this).data('_expanded', !$(this).data('_expanded'));
    });

});

function imgurThumb(url)
{
//if(url.match(/http:\/\/(?:i\.imgur\.com\/(.*?)\.(?:jpg|png|gif))$/))
//		url = url.substr(0, url.length - 4) + 'l' + url.substr(url.length - 4);
	
	return url;
}

function imgurDethumb(url)
{
//	if(url.match(/http:\/\/(?:i\.imgur\.com\/(.*?)\.(?:jpg|png|gif))$/))
//		url = url.substr(0, url.length - 5) + url.substr(url.length - 4);

	return url;
}