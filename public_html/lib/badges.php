<?php

/* This file should contain all functions and setup code for badge.
 * Right now I have a basic Proof of concept function. When we go 'feature complete'
 * we'll have a more defined library.  
 */

$__res = $sql->query("SELECT effect, effect_variable, user_id FROM badges RIGHT JOIN user_badges ON badges.id = user_badges.badge_id WHERE  badges.effect != 'NULL'");

//**TEMP: This function directly tests the database for if a perm exists and returns if it is or not.
function has_badge_perm($effectid, $userid = 0) {
	global $loguser, $config, $__res;
	if (!$config['badgesystem'])
		return false; //Break out of the function if disabled in the config

	if (!$userid)
		$userid = $loguser['id'];
	  foreach ($__res as $k => $v) {
	  if ($userid == $v['user_id']) {		
           if ($effectid == $v['effect']) {
			if ($v['effect_variable'] != NULL)
				return $v['effect_variable'];
			else
				return true;
		}
            }
	  }
        
	return false;
}

?>