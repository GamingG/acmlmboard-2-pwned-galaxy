$('input[type=file]').on('change', uploadFiles);

// Catch the form submit and upload the files
function uploadFiles(event)
{
  event.stopPropagation(); // Stop stuff happening
    event.preventDefault(); // Totally stop stuff happening

    // Create a formdata object and add the files
	var data = new FormData();
	$.each(event.target.files, function(key, value)
	{
		data.append(key, value);
	});
	
	startLoadingSpinner(); // must do this after or files gets wiped prematurely
    
    $.ajax({
	url: 'upload/image.php',
        type: 'POST',
        data: data,
        cache: false,
        dataType: 'json',
        processData: false, // Don't process the files
        contentType: false, // Set content type to false as jQuery will tell the server its a query string request
        success: function(data, textStatus, jqXHR)
        {
        	if(data.success)
        	{
        		// Success so call function to process the form
        		showImageURL(event, data);
        	}
        	else
        	{
        		// Handle errors here
			console.log('ERROR: API says: ' + data.error);
			alert('ERROR: API says: ' + data.error);
        	}
		
		stopLoadingSpinner();
        },
        error: function(jqXHR, textStatus, errorThrown)
        {
        	// Handle errors here
        	console.log('ERRORS: ' + textStatus);
		alert('ERRORS: ' + textStatus);
		
        	stopLoadingSpinner();
        }
    });
}

function showImageURL(event, data)
{
	var box = $('textarea');
	box.val(box.val() + ' [img]' + data.url + '[/img]');
}

function startLoadingSpinner()
{
	var input = $('#imgInput');
	input.val('');
	input.hide();
	input.after('<span id="imgSpinner"><img src="http://www.pwnedgalaxy.net/upload/loading.gif" alt="Uploading..." /></span>');
}

function stopLoadingSpinner()
{
	$('#imgInput').show();
	$('#imgSpinner').remove();
}

//$('#imgRow').show();