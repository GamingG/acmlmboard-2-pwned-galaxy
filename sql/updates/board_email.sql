#Adds a column for the email address that is shown to users who cannot register due to the proxy protection.
#Date: 12/15/2014
INSERT INTO `misc` (`field`, `txtval`) VALUES ('boardemail', 'admin@acmlm.org (This should be changed!)');