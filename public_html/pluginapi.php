<?php
//file_put_contents("pluginapi.txt", "hit ".time());

require_once "lib/common.php";
require_once "lib/hmacverify.php";
require_once "lib/pgconfig.php";
require_once "lib/minecraft.php";

$key = PG_CONFIG_KEY;

hmacverify($key, $_SERVER['HTTP_X_PG_SIGNATURE']);

file_put_contents("pluginapi.txt", var_export($_POST, 1));

function respondAndDie($msg, $mcid = "ALL")
{
	respond($msg, $mcid);
	die();
}

function respond($msg, $mcid = "ALL")
{
	echo "{$mcid};".MINECRAFT_COLOR_CHARACTER."b{$msg}\n";
}

//file_put_contents("pluginapi.txt", var_export($_POST, 1));

if($_POST['request'] == "command")
{
	$args = $_POST['args'];//(@$_POST['arg'] + @$_POST['args']);
	$mcid = str_replace("-", "", $_POST['mcid']);
	$command = array_shift($args);
	$ip = $_POST['remoteaddr'];
	
	switch($command)
	{
		case "link":
		case "login":
			if(count($args) == 1)
			{
				//echo "377102fc227e40ecae13cb5f0750d33d;{$args[0]}\n";
				//if($user = $sql->fetch($sql->query("SELECT * FROM `users` WHERE SHA1(CONCAT(`id`, `ip`))='".mysql_real_escape_string($args[0])."'")))
				if($userid = $sql->resultq("SELECT * FROM `users` WHERE SHA1(CONCAT(`id`, `ip`))='{$args[0]}'"))
				{
					if(sha1($userid.$ip) == $args[0] || sha1($userid.gethostbyname($ip)) == $args[0])
					{
						if($mcid2 = userIdToMCID($userid))
							respondAndDie("This forum account is already associated with ".minecraftIDToName($mcid2)." ({$mcid2})", $mcid);
						
						respondAndDie($sql->query("INSERT INTO `user_minecraft` (`uid`, `mcid`) VALUES ({$userid}, '{$mcid}')") ? "Successfully linked accounts" : "Unknown error", $mcid);
					}
					else
						respondAndDie("Could not verify IP address.  Either login to the forum again, or use /forum link USERNAME PASSWORD", $mcid);
				}
				else
					respondAndDie("Invalid token or invalid command format. ", $mcid);//"377102fc227e40ecae13cb5f0750d33d");
			}
			else
			{
				if($userid = checkuser($args[0], md5($pwdsalt2.$args[1].$pwdsalt)))
				{
					if($mcid2 = userIdToMCID($userid))
						respondAndDie("This forum account is already associated with ".minecraftIDToName($mcid2)." ({$mcid2})", $mcid);
					
					respondAndDie($sql->query("INSERT INTO `user_minecraft` (`uid`, `mcid`) VALUES ({$userid}, '{$mcid}')") ? "Successfully linked accounts" : "Unknown error", $mcid);
				}
				else
					respondAndDie("Incorrect password", $mcid);
			}
		break;
		case "sendprivate":
		case "mail":
			if(!count($args))
				respondAndDie("http://www.pwnedgalaxy.net/sendprivate.php", $mcid);
				
			$name = addslashes(array_shift($args));
			$msg = addslashes(implode(" ", $args));

			$userto = $sql->resultq("SELECT id FROM users WHERE name LIKE '{$name}' OR displayname LIKE '{$name}'");
			if(!$userto)
				$userto = $sql->resultq("SELECT uid FROM user_minecraft WHERE mcid='{$name}'");
			if(!$userto)
				$userto = $sql->resultq("SELECT uid FROM user_minecraft WHERE mcid='".minecraftNameToID($name)."'");
			
			if(!$userto)
				respondAndDie("No idea who you're talking about, bub. ):{", $mcid);
			
			if(!$msg)
				respondAndDie("http://www.pwnedgalaxy.net/sendprivate.php?uid={$userto}", $mcid);
			
			$userfrom = $sql->resultq("SELECT users.* FROM users INNER JOIN user_minecraft ON users.id=user_minecraft.uid WHERE mcid LIKE '{$mcid}'");
			
			if(!$userfrom)
				respondAndDie("Refusing to send a message on your behalf until you link your account. http://www.pwnedgalaxy.net/linkedaccounts.php", $mcid);
			
			$sql->query("INSERT INTO pmsgs (date,ip,userto,userfrom,unread,title) "
				    ."VALUES ('".ctime()."','".gethostbyname($ip)."',{$userto},{$userfrom},1,'Sent from Pwned Galaxy Minecraft Server')");
			$pid=$sql->insertid();
			$sql->query("INSERT INTO pmsgstext (id,text) VALUES ({$pid},'{$msg}')");
			
			respondAndDie($pid ? "Success! View here: http://www.pwnedgalaxy.net/showprivate.php?id={$pid}" : "Unknown error", $mcid);
		break;
	}
}
elseif($_POST['request'] == "heartbeat")
{
	if(count(@$_POST['mcid']))
	{
		$users = (array)$_POST['mcid'];
		
		foreach($users as $k => $v)
			$users[$k] = str_replace("-", "", $v);
		
		$ctime = ctime();
		$uidquery = "(SELECT `uid` AS `id` FROM `user_minecraft` WHERE `mcid` IN ('".implode("', '", $users)."'))";
		
		$threadq = $sql->query("SELECT `id`, `lastdate`, `title` FROM `threads` WHERE `lastdate`>=(SELECT MIN(GREATEST(`lastgamedate`, `lastview`)) FROM `users` WHERE `id` IN {$uidquery})");
		
		while($thread = $sql->fetch($threadq))
		{
			foreach($users as $mcid)
			{
				if($sql->resultq("SELECT 1 FROM users LEFT JOIN user_minecraft ON users.id=user_minecraft.uid WHERE mcid IS NULL OR (mcid LIKE '{$mcid}' AND {$thread['lastdate']}>=`lastgamedate` AND {$thread['lastdate']}>=`lastview`)"))
				{
					respond("New post!  Thread \"{$thread['title']}\":", $mcid);
					respond("http://pwnedgalaxy.net/thread.php?id={$thread['id']}", $mcid);
				}
			}
		}
		
		$pmsgq = $sql->query("SELECT `id`, `date`, `title`, `userto` FROM `pmsgs` WHERE `date`>=(SELECT MIN(GREATEST(`lastgamedate`, `lastview`)) FROM `users` WHERE `id` IN {$uidquery})");
		
		while($pmsg = $sql->fetch($pmsgq))
		{
			foreach($users as $mcid)
			{
				if($sql->resultq("SELECT 1 FROM users INNER JOIN user_minecraft ON users.id=user_minecraft.uid WHERE `id`={$pmsg['userto']} AND mcid LIKE '{$mcid}' AND {$pmsg['date']}>=`lastgamedate` AND {$pmsg['date']}>=`lastview`"))
				{
					respond("New PM! \"{$pmsg['title']}\":", $mcid);
					respond("http://www.pwnedgalaxy.net/showprivate.php?id={$pmsg['id']}", $mcid);
				}
			}
		}
		
		$sql->query("UPDATE `users` SET `lastgamedate`={$ctime}, `lastgamedesc`='Pwned Galaxy Minecraft Server' WHERE `id` IN {$uidquery}");
	}
		
}
// ALL;[yozorajp] I have entered the server, verily.w
?>
OK